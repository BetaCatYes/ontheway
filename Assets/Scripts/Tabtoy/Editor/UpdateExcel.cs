﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

public class UpdateExcel : MonoBehaviour
{
    [MenuItem("Custom/ExportExcelData")]
    static void ExportExcelData()
    {
        string workingDir = System.Environment.CurrentDirectory + "/GameConfig";
        string csPath = Application.dataPath + "/Scripts/Tabtoy/Config.cs";
        string binaryPath = Application.dataPath + "/Resources/Config.bytes";
        string excelFiles = "GlobalType.xlsx CardConfig.xlsx";
        var cmdStr = string.Format("tabtoy --mode=v2 --csharp_out={0} --binary_out={1} --combinename=Config {2}", csPath, binaryPath, excelFiles);
        var envVars = new List<string>()
        {
            "~/Code/go/bin"
        };
        ShellHelper.ProcessCommand(cmdStr, workingDir, envVars);
        AssetDatabase.ImportAsset("Assets/Resources/Config.bytes", ImportAssetOptions.ForceUpdate);
    }

    static OnTheWay.Config ReadExcelData()
    {
        TextAsset textAsset = Resources.Load("Config") as TextAsset;
        Stream stream = new MemoryStream(textAsset.bytes);
        var reader = new tabtoy.DataReader(stream);
        if (!reader.ReadHeader())
        {
            return null;
        }
        var config = new OnTheWay.Config();
        OnTheWay.Config.Deserialize(config, reader);
        return config;
    }
}
