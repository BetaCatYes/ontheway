﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using OnTheWay;
using System.IO;

public class PlayerDataManager : IManager
{
    CardGroupData cardGroupData;
    Config config;
    public void Init()
    {
        //读取Config配置
        TextAsset textAsset = Resources.Load("Config") as TextAsset;
        Stream stream = new MemoryStream(textAsset.bytes);
        var reader = new tabtoy.DataReader(stream);
        // if (!reader.ReadHeader())
        // {
        //     Debug.LogError("Can't load config.");
        // }
        config = new Config();
        Config.Deserialize(config, reader);
    }

    public void Release()
    {
    }

    public void Update()
    {
    }
    public PlayerData GetPlayerData()
    {
        var playeData = new PlayerData();
        playeData.cardGroupData = cardGroupData;
        playeData.energyMax = 3;
        return playeData;
    }
    public PlayerData GetPlayerData(CharacterType characterType)
    {
        var defines = config.CardConfig.FindAll((d) => { return d.Character == characterType; });
        var cgd = new CardGroupData();
        foreach (var define in defines)
        {
            var card = CardData.Deserialize(define.Card);
            cgd.stackCards.Add(card);
        }
        var playeData = new PlayerData();
        playeData.cardGroupData = cgd;
        playeData.energyMax = 3;
        return playeData;
    }
    public void AddCard(CardData card)
    {
        cardGroupData.stackCards.Add(card);
    }
}
