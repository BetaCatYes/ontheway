﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoroutineAgent : MonoBehaviour
{
    static CoroutineAgent singleton;
    public static TaskState CreateTask(bool endless = false)
    {
        if (singleton == null)
        {
            GameObject go = new GameObject("CoroutineAgent");
            GameObject.DontDestroyOnLoad(go);
            singleton = go.AddComponent<CoroutineAgent>();
        }
        return new TaskState(endless);
    }
    public class TaskState
    {
        public bool Running
        {
            get
            {
                return running;
            }
        }

        public bool Paused
        {
            get
            {
                return paused;
            }
        }

        public event System.Action<bool> OnFinished;

        List<IEnumerator> coroutines;
        bool endless;
        bool running;
        bool paused;
        bool stopped;

        public TaskState(bool endless)
        {
            coroutines = new List<IEnumerator>();
            this.endless = endless;
            singleton.StartCoroutine(CallWrapper());
        }

        public void Push(IEnumerator coroutine)
        {
            coroutines.Add(coroutine);
        }
        public void Pause()
        {
            paused = true;
        }

        public void Unpause()
        {
            paused = false;
        }

        public void Stop()
        {
            stopped = true;
            running = false;
        }
        IEnumerator CallWrapper()
        {
            do
            {
                running = true;
                stopped = false;
                IEnumerator e = null;
                while (coroutines.Count == 0)//等待
                {
                    yield return null;
                }
                while (running)
                {
                    if (paused)
                        yield return null;
                    else
                    {
                        if (e != null && e.MoveNext())
                        {
                            yield return e.Current;
                        }
                        else if (coroutines.Count > 0)
                        {
                            e = coroutines[0];
                            coroutines.RemoveAt(0);
                        }
                        else
                        {
                            running = false;
                        }
                    }
                }

                if (OnFinished != null)
                {
                    OnFinished.Invoke(stopped);
                }
            } while (endless);
        }
    }
}
