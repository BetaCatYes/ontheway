﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IManager
{
    void Init();
    void Release();
    void Update();
}
public class GameManager : MonoBehaviour
{
    static List<IManager> managerPool;
    public static T GetManager<T>() where T : class
    {
        if (!Application.isPlaying)
            return null;
        if (managerPool == null)
        {
            var prefab = Resources.Load<GameObject>("GameManager");
            var go = Instantiate<GameObject>(prefab);
            DontDestroyOnLoad(go);
        }
        foreach (var mgr in managerPool)
        {
            if (mgr is T)
            {
                return mgr as T;
            }
        }
        return null;
    }

    void Awake()
    {
        //Manager统一初始化
        managerPool = new List<IManager>();

        var processManager = new ViewProcessManager();
        managerPool.Add(processManager);

        var battleManager = new BattleManager();
        managerPool.Add(battleManager);

        var playerDataManager = new PlayerDataManager();
        managerPool.Add(playerDataManager);

        foreach (var mgr in managerPool)
        {
            mgr.Init();
        }
    }

    void OnDestroy()
    {
        foreach (var mgr in managerPool)
        {
            mgr.Release();
        }
    }

    void Update()
    {
        foreach (var mgr in managerPool)
        {
            mgr.Update();
        }
    }
}
