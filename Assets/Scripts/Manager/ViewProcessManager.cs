﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
//更新View使用
public class ViewProcessManager : IManager
{
    CoroutineAgent.TaskState task;
    public event System.Action OnFinished;
    public void Init()
    {
        task = CoroutineAgent.CreateTask(true);
        task.OnFinished +=
            (s) =>
            {
                if (OnFinished != null)
                {
                    OnFinished.Invoke();
                    OnFinished = null;
                }
            };
    }
    public void Release() { }
    public void Update() { }
    public void Push(ViewProcess process)
    {
        task.Push(process.Processing());
    }
}
public abstract class ViewProcess
{
    public abstract IEnumerator Processing();
}
public class MoveProcess : ViewProcess
{
    Transform transform;
    int posIndex;
    public MoveProcess(Transform transform, int posIndex)
    {
        this.transform = transform;
        this.posIndex = posIndex;
    }
    public override IEnumerator Processing()
    {
        var blockView = GameManager.GetManager<BattleManager>().MapView.blockViews[posIndex];
        transform.DOMove(blockView.transform.position, 0.5f).SetEase(Ease.Linear);
        yield return new WaitForSeconds(0.5f);
    }
}
public class MoveStartProcess : ViewProcess
{
    public override IEnumerator Processing()
    {
        yield return 0;
    }
}
public class MoveEndProcess : ViewProcess
{
    Transform transform;
    int posIndex;
    public MoveEndProcess(Transform transform, int posIndex)
    {
        this.transform = transform;
        this.posIndex = posIndex;
    }
    public override IEnumerator Processing()
    {
        var blockView = GameManager.GetManager<BattleManager>().MapView.blockViews[posIndex];
        yield return new WaitForSeconds(0.5f);
    }
}
public class RoadSignProcess : ViewProcess
{
    BlockView blockView;
    int targetCount;
    public RoadSignProcess(BlockView blockView, int targetCount)
    {
        this.blockView = blockView;
        this.targetCount = targetCount;
    }
    public override IEnumerator Processing()
    {
        blockView.UpdateRoadSignView(targetCount);
        yield return new WaitForSeconds(0.5f);
    }
}
public class GetStarProcess : ViewProcess
{
    BlockView blockView;
    int starCount;
    public GetStarProcess(BlockView blockView, int starCount)
    {
        this.blockView = blockView;
        this.starCount = starCount;
    }
    public override IEnumerator Processing()
    {
        blockView.UpdateStarView(starCount);
        yield return new WaitForSeconds(0.5f);
    }
}
public class GetFireProcess : ViewProcess
{
    BlockView blockView;
    PlayerData playerData;
    public GetFireProcess(BlockView blockView, PlayerData playerData)
    {
        this.blockView = blockView;
        this.playerData = playerData;
    }
    public override IEnumerator Processing()
    {
        yield return new WaitForSeconds(0.5f);
    }
}
public class MapUpdateProcess : ViewProcess
{
    BlockView[] blockViews;
    int[] posIndexs;
    int[] roadSignCounts;
    int[] starCounts;
    int[] fireCounts;
    public MapUpdateProcess(BlockView[] blockViews, int[] posIndexs, int[] roadSignCounts, int[] starCounts, int[] fireCounts)
    {
        this.blockViews = blockViews;
        this.posIndexs = posIndexs;
        this.roadSignCounts = roadSignCounts;
        this.starCounts = starCounts;
        this.fireCounts = fireCounts;
    }
    public override IEnumerator Processing()
    {
        for (int i = 0; i < posIndexs.Length; i++)
        {
            var view = blockViews[posIndexs[i]];
            view.UpdateRoadSignView(roadSignCounts[i]);
            view.UpdateStarView(starCounts[i]);
            view.UpdateFireView(fireCounts[i]);
        }
        yield return new WaitForSeconds(0.5f);
    }
}
public class DrawCardsProcess : ViewProcess
{
    CardSlotView cardSlotView;
    List<CardData> cards = new List<CardData>();
    public DrawCardsProcess(CardSlotView cardSlotView, List<CardData> cards)
    {
        this.cardSlotView = cardSlotView;
        this.cards.AddRange(cards);
    }
    public override IEnumerator Processing()
    {
        for (int i = 0; i < cards.Count; i++)
        {
            var cardView = cardSlotView.NewCardView();
            cardView.Setup(cards[i]);
            yield return new WaitForSeconds(1);
        }
    }
}