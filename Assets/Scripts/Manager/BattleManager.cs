﻿using System.Collections;
using System.Collections.Generic;
using OnTheWay;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BattleManager : IManager
{
    public event System.Action OnPlayerRoundStart;
    public event System.Action OnPlayerRoundEnd;
    public bool IsMainPlayerRound { get; private set; }
    public MapView MapView { get; private set; }
    public UIBattle UIBattle { get; private set; }
    public bool IsBattleStart { get; private set; }
    public bool IsBattlePause { get; private set; }
    BattleData battleData;
    CoroutineAgent.TaskState taskState;
    public void Init()
    {
    }

    public void Release()
    {
    }

    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.F1))
            BattleStart();
    }
    #region BattleProcess 
    public void BattleStart()
    {
        MapView = GameObject.FindGameObjectWithTag("Map").GetComponent<MapView>();
        MapView.Setup(MapView.mapData);

        var playerDataManager = GameManager.GetManager<PlayerDataManager>();
        var playerDatas = new List<PlayerData>();
        var heroPlayerData = playerDataManager.GetPlayerData(CharacterType.Player);
        var bossPlayerData = playerDataManager.GetPlayerData(CharacterType.Boss);
        playerDatas.Add(heroPlayerData);
        playerDatas.Add(bossPlayerData);

        var players = GameObject.FindGameObjectsWithTag("Player");
        players[0].GetComponent<PlayerView>().Setup(heroPlayerData);
        players[1].GetComponent<PlayerView>().Setup(bossPlayerData);

        var cardSlotView = GameObject.FindGameObjectWithTag("CardSlot").GetComponent<CardSlotView>();
        cardSlotView.Setup(playerDatas[0].cardGroupData);

        UIBattle = GameObject.FindGameObjectWithTag("UI").GetComponent<UIBattle>();
        UIBattle.Setup(playerDatas[0]);

        IsBattleStart = true;
        battleData = new BattleData(MapView.mapData, playerDatas);
        battleData.RoundStart();
        taskState = CoroutineAgent.CreateTask();
        taskState.Push(battleData.BattleExecute());
        PlayerRoundStart();
    }
    public void BattlePause()
    {
        IsBattlePause = true;
        taskState.Pause();
    }
    public void BattleResume()
    {
        IsBattlePause = false;
        taskState.Unpause();
    }
    public void BattleEnd()
    {
        IsBattleStart = false;
        taskState.Stop();
    }
    public void PlayerRoundStart()
    {
        IsMainPlayerRound = true;
        if (OnPlayerRoundStart != null)
            OnPlayerRoundStart.Invoke();
    }
    public void PlayerRoundEnd()
    {
        IsMainPlayerRound = false;
        if (OnPlayerRoundEnd != null)
            OnPlayerRoundEnd.Invoke();
    }
    #endregion
    #region BattleViewInterface
    public bool CanCardEffect(CardData cardData)
    {
        return battleData.CanCardEffect(cardData);
    }
    public void CardEffect(CardData cardData, BlockData blockData)
    {
        battleData.CardEffect(cardData, blockData);
    }
    public void RoundEnd()
    {
        battleData.RoundEnd();
        if (battleData.IsMainPlayer)
            PlayerRoundStart();
        else
            PlayerRoundEnd();
    }
    #endregion
}
