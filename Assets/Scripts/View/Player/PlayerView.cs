﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PlayerView : MonoBehaviour
{
    public PlayerData playerData;
    public void Setup(PlayerData playerData)
    {
        this.playerData = playerData;
        playerData.OnMoveStart += (p) =>
        {
            GameManager.GetManager<ViewProcessManager>().Push(new MoveStartProcess());
        };
        playerData.OnMovingEnd += (p, stepNumber, mapData, pd) =>
        {
            GameManager.GetManager<ViewProcessManager>().Push(new MoveProcess(transform, p));
        };
        playerData.OnMoveEnd += (p) =>
        {
            GameManager.GetManager<ViewProcessManager>().Push(new MoveEndProcess(transform, p));
        };
    }
}