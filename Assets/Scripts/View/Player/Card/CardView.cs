﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardView : MonoBehaviour
{
    public event System.Action OnEffect;
    public Text text;
    CardData cardData;
    public void Setup(CardData cardData)
    {
        this.cardData = cardData;
        text.text = cardData.ToString();
        GetComponent<Button>().onClick.AddListener(
            () =>
            {
                var battleManager = GameManager.GetManager<BattleManager>();
                if (!battleManager.CanCardEffect(cardData) || !battleManager.IsMainPlayerRound)
                    return;

                if (cardData.selectRange != 0)
                {
                    SelectBlock(cardData.selectRange, (blockData) =>
                    {
                        CardEffect(blockData);
                    });
                }
                else
                {
                    CardEffect();
                }
            });
    }
    void CardEffect(BlockData blockData = null)
    {
        var battleManager = GameManager.GetManager<BattleManager>();
        battleManager.CardEffect(cardData, blockData);
        if (OnEffect != null)
            OnEffect.Invoke();
    }
    void SelectBlock(int range, System.Action<BlockData> callback)
    {

    }
}