﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardSlotView : MonoBehaviour
{
    public GameObject cardPrefab;
    public List<CardView> cardViews;
    CardGroupData cardGroupData;
    public void Setup(CardGroupData cardGroupData)
    {
        this.cardGroupData = cardGroupData;
        cardGroupData.OnDrawCard += (cards) =>
        {
            GameManager.GetManager<ViewProcessManager>().Push(new DrawCardsProcess(this, cards));
        };
    }
    public CardView NewCardView()
    {
        var go = Instantiate(cardPrefab, Vector3.zero, Quaternion.identity, transform);
        var cardView = go.GetComponent<CardView>();
        cardView.OnEffect += () =>
        {
            cardViews.Remove(cardView);
            Destroy(cardView.gameObject);//TODO:消失动画
        };
        cardViews.Add(cardView);
        return cardView;
    }
}
