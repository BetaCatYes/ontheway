﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class MapView : MonoBehaviour
{
    public List<BlockView> blockViews;
    public MapData mapData;
    void Awake()
    {
        mapData = new MapData();
        mapData.blockDatas = new BlockData[blockViews.Count];
        for (int i = 0; i < blockViews.Count; i++)
        {
            mapData.blockDatas[i] = blockViews[i].blockData;
        }
    }
    public void Setup(MapData mapData)
    {
        this.mapData = mapData;
        for (int i = 0; i < mapData.blockDatas.Length; i++)
        {
            blockViews[i].Setup(mapData.blockDatas[i]);
        }
        mapData.OnBlocksChange += (ps, rc, sc, fc) =>
        {
            GameManager.GetManager<ViewProcessManager>().Push(new MapUpdateProcess(blockViews.ToArray(), ps, rc, sc, fc));
        };
    }
#if UNITY_EDITOR
    [Space(10), Header("EDITOR")]
    public bool isDraw = true;
    private void OnDrawGizmos()
    {
        if (Application.isPlaying) return;
        blockViews = GetComponentsInChildren<BlockView>().ToList();
        blockViews.Sort((x, y) => x.transform.position.x.CompareTo(y.transform.position.x));
        for (int i = 0; i < blockViews.Count; i++)
        {
            blockViews[i].transform.SetSiblingIndex(i);
            blockViews[i].blockData.index = i;
            if (!isDraw) continue;
            Gizmos.color = Color.red;
            Gizmos.DrawSphere(blockViews[i].transform.position, 0.1f);
            Gizmos.color = Color.black;
            if (i != blockViews.Count - 1)
                Gizmos.DrawLine(blockViews[i].transform.position, blockViews[i + 1].transform.position);
        }
    }
#endif
}

