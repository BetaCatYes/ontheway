﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class BlockView : MonoBehaviour
{
    public BlockData blockData;
    Animator animator;
    void Awake()
    {
        animator = GetComponent<Animator>();
    }
    public void Setup(BlockData blockData)
    {
        this.blockData = blockData;
        UpdateStarView(blockData.StarCount);
        UpdateRoadSignView(blockData.RoadSignCount);
        blockData.OnRoadSignChange += (count) =>
        {
            GameManager.GetManager<ViewProcessManager>().Push(new RoadSignProcess(this, count));
        };
        blockData.OnStarChange += (count) =>
        {
            GameManager.GetManager<ViewProcessManager>().Push(new GetStarProcess(this, count));
        };
    }
    public void UpdateRoadSignView(int roadSignCount)
    {
        animator.SetInteger("RoadSign", roadSignCount);
    }
    public void UpdateStarView(int starCount)
    {
        animator.SetInteger("Star", starCount);
    }
    public void UpdateFireView(int fireCount)
    {
        animator.SetInteger("Fire", fireCount);
    }
}