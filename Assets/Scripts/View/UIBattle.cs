﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIBattle : MonoBehaviour
{
    public Button roundEndButton;
    public Text energyText;
    PlayerData playerData;
    public void Setup(PlayerData playerData)
    {
        this.playerData = playerData;

        var battleManager = GameManager.GetManager<BattleManager>();
        battleManager.OnPlayerRoundStart += () =>
        {
            roundEndButton.gameObject.SetActive(true);
        };
        battleManager.OnPlayerRoundEnd += () =>
        {
            roundEndButton.gameObject.SetActive(false);
        };
        roundEndButton.onClick.AddListener(() =>
        {
            battleManager.RoundEnd();
        });

        playerData.OnEnergyChange += (count) =>
        {
            energyText.text = count + "";
        };
    }
}
