﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using SimpleJSON;
using System;

public class MultiPlatformFileIO
{
    public static void Save(string file, JSONNode json)
    {
        json = json ?? new JSONObject();
        DefaultSave(file, json);
    }
    //没有存则返回new JSONObject()
    public static JSONNode Load(string file)
    {
        JSONObject json = null;
        json = DefaultLoad(file).AsObject;
        return json;
    }

    public static void Clear(string file)
    {
        Save(file, null);
    }

    private static void DefaultSave(string file, JSONNode json)
    {
        var filePath = Application.persistentDataPath + Path.DirectorySeparatorChar + file;
        json.SaveToBinaryFile(filePath);
    }
    private static JSONNode DefaultLoad(string file)
    {
        var filePath = Application.persistentDataPath + Path.DirectorySeparatorChar + file;
        if (File.Exists(filePath))
            return JSONNode.LoadFromBinaryFile(filePath);
        return new JSONObject();
    }
}
