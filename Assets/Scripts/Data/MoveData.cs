﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveData
{
    public static void Effect(int? stepNumber, MapData mapData, PlayerData playerData)
    {
        if (stepNumber > 0)
        {
            playerData.MoveStart(stepNumber, mapData);
            while (stepNumber > 0)
            {
                stepNumber--;
                if (mapData.blockDatas[playerData.posIndex].Pass(stepNumber, mapData, playerData))
                    playerData.Move(stepNumber, mapData);
                else
                    playerData.RoadSignStop(stepNumber, mapData);
            }
            playerData.MoveEnd();
            mapData.blockDatas[playerData.posIndex].Stay(mapData, playerData);
        }
    }
}
