﻿using System.Collections;
using System.Collections.Generic;
using SimpleJSON;
using UnityEngine;

public class BattleData
{
    public int currentRoundIndex;
    public MapData mapData;
    public List<PlayerData> playerDatas = new List<PlayerData>();
    public int PlayerIndex
    {
        get
        {
            return currentRoundIndex % playerDatas.Count;
        }
    }
    public PlayerData PlayerData
    {
        get
        {
            return playerDatas[PlayerIndex];
        }
    }
    public bool IsMainPlayer
    {
        get
        {
            return PlayerIndex == 0;
        }
    }
    //public bool IsRoundRunning { get; set; }
    public BattleData()
    {

    }
    public BattleData(MapData mapData, List<PlayerData> playerDatas)
    {
        this.mapData = mapData;
        this.playerDatas = playerDatas;
        playerDatas.ForEach((p) => p.Init());
    }
    public IEnumerator BattleExecute()
    {
        while (true)
        {
            if (IsMainPlayer)
                PlayerData.RoundExecuteHuman();
            else
                PlayerData.RoundExecuteAI(this);
            yield return 0;
        }
    }
    public bool CanCardEffect(CardData cardData)
    {
        return PlayerData.CanEffectCard(cardData);
    }
    public void CardEffect(CardData cardData, BlockData blockData)
    {
        PlayerData.EffectCard(cardData, blockData, mapData);
    }
    public void RoundStart()
    {
        PlayerData.RoundStart();
    }
    public void RoundEnd()
    {
        PlayerData.RoundEnd();
        currentRoundIndex++;
        RoundStart();

        if (IsMainPlayer)
        {
            mapData.DoubleRoundEnd();
        }
    }
    #region Serialize
    public JSONNode Serialize()
    {
        var json = new JSONObject();
        json["currentRoundIndex"] = currentRoundIndex;
        json["mapData"] = mapData.Serialize();

        var jsonPlayerDatas = new JSONArray();
        for (int i = 0; i < playerDatas.Count; i++)
        {
            jsonPlayerDatas[i] = playerDatas[i].Serialize();
        }

        json["playerDatas"] = jsonPlayerDatas;

        return json;
    }
    public static BattleData Deserialize(JSONNode json)
    {
        var battleData = new BattleData();
        battleData.currentRoundIndex = json["currentRoundIndex"];
        battleData.mapData = MapData.Deserialize(json["mapData"]);

        var jsonPlayerDatas = json["playerDatas"];
        for (int i = 0; i < jsonPlayerDatas.Count; i++)
        {
            battleData.playerDatas.Add(PlayerData.Deserialize(jsonPlayerDatas[i]));
        }

        return battleData;
    }
    #endregion
}
