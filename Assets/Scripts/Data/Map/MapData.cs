﻿using System.Collections;
using System.Collections.Generic;
using SimpleJSON;
using UnityEngine;
public class MapData
{
    public event System.Action<int[], int[], int[], int[]> OnBlocksChange;
    public BlockData[] blockDatas;
    public BlockData GetBlockDataByIndex(int index)
    {
        if (index < 0 || index > blockDatas.Length - 1)
        {
            return null;
        }
        return blockDatas[index];
    }
    public void DoubleRoundEnd()
    {
        int[] posIndexs = new int[blockDatas.Length];
        int[] roadSigns = new int[blockDatas.Length];
        int[] stars = new int[blockDatas.Length];
        int[] fires = new int[blockDatas.Length];
        for (int i = 0; i < blockDatas.Length; i++)
        {
            roadSigns[i] = Mathf.Max(0, blockDatas[i].RoadSignCount - 1);
            stars[i] = blockDatas[i].StarCount;
            fires[i] = Mathf.Max(0, blockDatas[i].FireCount - 1);
        }
        BlocksChange(posIndexs, roadSigns, stars, fires);
    }
    public void BlocksChange(int[] posIndexs, int[] roadSigns, int[] stars, int[] fires)
    {
        #region Null初始化
        if (roadSigns == null)
        {
            roadSigns = new int[posIndexs.Length];
            for (int i = 0; i < blockDatas.Length; i++)
            {
                roadSigns[i] = blockDatas[posIndexs[i]].RoadSignCount;
            }
        }
        if (stars == null)
        {
            stars = new int[posIndexs.Length];
            for (int i = 0; i < blockDatas.Length; i++)
            {
                stars[i] = blockDatas[posIndexs[i]].StarCount;
            }
        }
        if (fires == null)
        {
            fires = new int[posIndexs.Length];
            for (int i = 0; i < blockDatas.Length; i++)
            {
                fires[i] = blockDatas[posIndexs[i]].FireCount;
            }
        }
        #endregion
        for (int i = 0; i < blockDatas.Length; i++)
        {
            blockDatas[posIndexs[i]].roadSignCount = roadSigns[i];
            blockDatas[posIndexs[i]].starCount = stars[i];
            blockDatas[posIndexs[i]].fireCount = fires[i];
        }
        if (OnBlocksChange != null)
        {
            OnBlocksChange.Invoke(posIndexs, roadSigns, stars, fires);
        }
    }

    #region Serialize
    public JSONNode Serialize()
    {
        JSONArray json = new JSONArray();
        for (int i = 0; i < blockDatas.Length; i++)
        {
            json[i] = blockDatas[i].Serialize();
        }
        return json;
    }
    public static MapData Deserialize(JSONNode json)
    {
        var mapData = new MapData();
        mapData.blockDatas = new BlockData[json.Count];
        for (int i = 0; i < json.Count; i++)
        {
            var block = BlockData.Deserialize(json[i]);
            mapData.blockDatas[i] = block;
        }
        return mapData;
    }
    #endregion
}
