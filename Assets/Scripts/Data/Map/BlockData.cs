﻿using System.Collections;
using System.Collections.Generic;
using SimpleJSON;
using UnityEngine;
[System.Serializable]
public class BlockData
{
    public event System.Action<int> OnRoadSignChange;
    public event System.Action<int> OnStarChange;
    public event System.Action<int> OnFireChange;
    public int index;
    [SerializeField]
    public int roadSignCount;
    public int RoadSignCount
    {
        get { return roadSignCount; }
        set
        {
            roadSignCount = value;
            if (OnRoadSignChange != null)
            {
                OnRoadSignChange.Invoke(roadSignCount);
            }
        }
    }
    [SerializeField]
    public int starCount;
    public int StarCount
    {
        get { return starCount; }
        set
        {
            starCount = value;
            if (OnStarChange != null)
                OnStarChange.Invoke(starCount);
        }
    }
    [SerializeField]
    public int fireCount;
    public int FireCount
    {
        get { return fireCount; }
        set
        {
            fireCount = value;
            if (OnFireChange != null)
                OnFireChange.Invoke(fireCount);
        }
    }
    public bool Pass(int? stepNumber, MapData mapData, PlayerData playerData)
    {
        if (RoadSignCount > 0)
        {
            RoadSignCount--;
            return false;
        }
        return true;
    }
    public void Stay(MapData mapData, PlayerData playerData)
    {
        if (StarCount > 0)
        {
            StarCount--;
            playerData.GetStar(mapData);
        }
        if (fireCount > 0)
        {
            playerData.GetFire(mapData);
        }
    }

    #region Serialize
    public JSONNode Serialize()
    {
        JSONObject json = new JSONObject();
        json["index"] = index;
        json["roadSignCount"] = roadSignCount;
        json["starCount"] = starCount;
        json["fireCount"] = fireCount;
        return json;
    }
    public static BlockData Deserialize(JSONNode json)
    {
        var blockData = new BlockData();
        blockData.index = json["index"];
        blockData.roadSignCount = json["roadSignCount"];
        blockData.starCount = json["starCount"];
        blockData.fireCount = json["fireCount"];
        return blockData;
    }
    #endregion
}