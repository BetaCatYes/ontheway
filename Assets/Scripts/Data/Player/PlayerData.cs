﻿using System.Collections;
using System.Collections.Generic;
using SimpleJSON;
using UnityEngine;

[System.Serializable]
public class PlayerData
{
    public CardGroupData cardGroupData;
    public event System.Action<int> OnMoveStart;
    public event System.Action<int, int?, MapData, PlayerData> OnMovingStart;
    public event System.Action<int, int?, MapData, PlayerData> OnMovingEnd;
    public event System.Action<int, int?, MapData, PlayerData> OnRoadSignStop;
    public event System.Action<int> OnMoveEnd;
    public event System.Action<int> OnEnergyChange;
    public event System.Action<MapData, PlayerData> OnGetStar;
    public event System.Action<PlayerData> OnRoundStart;
    public event System.Action<PlayerData> OnRoundEnd;
    public int posIndex;
    public int energyMax;
    protected int energy;
    public int Energy
    {
        get { return energy; }
        set
        {
            energy = value;
            if (OnEnergyChange != null)
                OnEnergyChange.Invoke(energy);
        }
    }
    public int moveIncreaseSpeed;
    public bool IsRounding { get; private set; }
    public bool IsWaitingRoundEnd { get; private set; }
    public void Init()
    {
        cardGroupData.Init();
        moveIncreaseSpeed = posIndex = 0;
        Energy = energyMax;
    }
    #region ROUND
    public void RoundStart()
    {
        Energy = energyMax;
        cardGroupData.DrawCards(5);
        IsRounding = true;
        if (OnRoundStart != null)
        {
            OnRoundStart.Invoke(this);
        }

        IsWaitingRoundEnd = false;
    }
    public void RoundExecuteHuman()
    {

    }
    public void RoundExecuteAI(BattleData battleData)
    {
        if (IsWaitingRoundEnd) return;

        var battleManager = GameManager.GetManager<BattleManager>();
        var indexList = new List<int>();
        var selectList = new List<int>();
        var aiData = new AIData();
        aiData.Select(battleData, 1, indexList, selectList);
        var handCards = new List<CardData>();
        handCards.AddRange(cardGroupData.handCards);
        for (int i = 0; i < indexList.Count; i++)
        {
            if (battleManager.CanCardEffect(handCards[indexList[i]]))
            {
                var block = battleData.mapData.GetBlockDataByIndex(selectList[i] + battleData.PlayerData.posIndex);
                if (block != null)
                    battleManager.CardEffect(handCards[indexList[i]], block);
            }
        }
        GameManager.GetManager<ViewProcessManager>().OnFinished += () =>
        {
            battleManager.RoundEnd();
        };
        IsWaitingRoundEnd = true;
    }
    public void RoundEnd()
    {
        IsRounding = false;
        if (OnRoundEnd != null)
            OnRoundEnd.Invoke(this);
    }
    #endregion
    #region EFFECT CARD
    public bool CanEffectCard(CardData cardData)
    {
        return Energy >= cardData.energyCost;
    }
    public void EffectCard(CardData cardData, BlockData blockData, MapData mapData)
    {
        Energy -= cardData.energyCost;
        cardGroupData.EffectCard(cardData, blockData, mapData, this);
    }
    #endregion
    #region MOVE
    public void MoveStart(int? stepNumber, MapData mapData)
    {
        stepNumber += moveIncreaseSpeed;
        if (OnMoveStart != null)
            OnMoveStart.Invoke(posIndex);
    }
    public void Move(int? stepNumber, MapData mapData)
    {
        if (OnMovingStart != null)
            OnMovingStart.Invoke(posIndex, stepNumber, mapData, this);
        posIndex++;
        if (OnMovingEnd != null)
            OnMovingEnd.Invoke(posIndex, stepNumber, mapData, this);
    }
    public void RoadSignStop(int? stepNumber, MapData mapData)
    {
        if (OnRoadSignStop != null)
            OnRoadSignStop.Invoke(posIndex, stepNumber, mapData, this);
    }
    public void MoveEnd()
    {
        if (OnMoveEnd != null)
            OnMoveEnd.Invoke(posIndex);
    }
    #endregion
    #region GET SOMETHINE
    public void GetStar(MapData mapData)
    {
        Energy++;
        if (OnGetStar != null)
            OnGetStar.Invoke(mapData, this);
    }
    public void GetSpeed()
    {
        moveIncreaseSpeed++;
    }
    public void GetFire(MapData mapData)
    {
        if (Energy > 0)
            Energy--;
    }
    #endregion
    #region BUFF
    List<Buff> buffList = new List<Buff>();
    public void AddBuff(Buff buff)
    {
        buff.OnCast(this);
        buffList.Add(buff);
    }
    public void RemoveBuff(Buff buff)
    {
        buff.OnRemove(this);
        buffList.Remove(buff);
    }
    #endregion

    #region Serialize
    public JSONNode Serialize()
    {
        var json = new JSONObject();

        json["cardGroupData"] = cardGroupData.Serialize();

        var jsonBuffList = new JSONArray();
        for (int i = 0; i < buffList.Count; i++)
        {
            jsonBuffList[i] = buffList[i].Serialize();
        }
        json["buffList"] = jsonBuffList;

        json["posIndex"] = posIndex;
        json["energy"] = energy;
        json["energyMax"] = energyMax;
        json["moveIncreaseSpeed"] = moveIncreaseSpeed;
        return json;
    }
    public static PlayerData Deserialize(JSONNode json)
    {
        var playerData = new PlayerData();
        playerData.cardGroupData = CardGroupData.Deserialize(json["cardGroupData"]);

        var jsonBuffList = json["buffList"];
        for (int i = 0; i < jsonBuffList.Count; i++)
        {
            var buff = Buff.Deserialize(jsonBuffList[i]);
            playerData.AddBuff(buff);
        }

        playerData.posIndex = json["posIndex"];
        playerData.energy = json["energy"];
        playerData.energyMax = json["energyMax"];
        playerData.moveIncreaseSpeed = json["pomoveIncreaseSpeedsIndex"];

        return playerData;
    }
    #endregion
}