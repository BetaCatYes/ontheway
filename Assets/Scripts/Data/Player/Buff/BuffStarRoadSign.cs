﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuffStarRoadSign : Buff
{
    public override void OnCast(PlayerData playerData)
    {
        base.OnCast(playerData);
        playerData.OnGetStar += StarRoadSignEffect;
    }
    public override void OnRemove(PlayerData playerData)
    {
        base.OnRemove(playerData);
        playerData.OnGetStar -= StarRoadSignEffect;
    }
    void StarRoadSignEffect(MapData mapData, PlayerData playerData)
    {
        mapData.blockDatas[playerData.posIndex - 1].RoadSignCount++;
    }
}
