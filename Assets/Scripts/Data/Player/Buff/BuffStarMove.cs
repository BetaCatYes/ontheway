﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuffStarMove : Buff
{
    public override void OnCast(PlayerData playerData)
    {
        base.OnCast(playerData);
        playerData.OnGetStar += StarMoveEffect;
    }
    public override void OnRemove(PlayerData playerData)
    {
        base.OnRemove(playerData);
        playerData.OnGetStar -= StarMoveEffect;
    }
    void StarMoveEffect(MapData mapData, PlayerData playerData)
    {
        MoveData.Effect(1, mapData, playerData);
    }
}
