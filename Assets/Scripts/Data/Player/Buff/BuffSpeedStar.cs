﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuffSpeedStar : Buff
{
    public override void OnCast(PlayerData playerData)
    {
        base.OnCast(playerData);
        playerData.OnGetStar += SpeedRoadSignEffect;
    }
    public override void OnRemove(PlayerData playerData)
    {
        base.OnRemove(playerData);
        playerData.OnGetStar -= SpeedRoadSignEffect;
    }
    void SpeedRoadSignEffect(MapData mapData, PlayerData playerData)
    {
        playerData.AddBuff(new BuffSpeed(1, 1));
    }
}

