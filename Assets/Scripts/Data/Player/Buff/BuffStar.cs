﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuffStar : Buff
{
    public BuffStar()
    {
        RoundDuration = 1;
    }
    public override void OnCast(PlayerData playerData)
    {
        base.OnCast(playerData);
        playerData.OnMovingEnd += StarEffect;
    }
    public override void OnRemove(PlayerData playerData)
    {
        base.OnRemove(playerData);
        playerData.OnMovingEnd -= StarEffect;
    }
    void StarEffect(int posIndex, int? stepNumber, MapData mapData, PlayerData playerData)
    {
        if (mapData.blockDatas[posIndex].StarCount > 0)
            stepNumber = 0;
    }
}
