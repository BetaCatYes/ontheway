﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuffSpeedRoadSign : Buff
{
    public override void OnCast(PlayerData playerData)
    {
        base.OnCast(playerData);
        playerData.OnRoadSignStop += SpeedRoadSignEffect;
    }
    public override void OnRemove(PlayerData playerData)
    {
        base.OnRemove(playerData);
        playerData.OnRoadSignStop -= SpeedRoadSignEffect;
    }
    void SpeedRoadSignEffect(int posIndex, int? stepIndex, MapData mapData, PlayerData playerData)
    {
        playerData.AddBuff(new BuffSpeed(1, 1));
    }
}

