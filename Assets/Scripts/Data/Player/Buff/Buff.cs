﻿using System.Collections;
using System.Collections.Generic;
using SimpleJSON;
using UnityEngine;

public class Buff
{
    protected int RoundDuration { get; set; }
    int roundTimer;
    public virtual void OnCast(PlayerData playerData)
    {
        playerData.OnRoundStart += BuffLifeCount;
    }
    public virtual void OnRemove(PlayerData playerData)
    {
        playerData.OnRoundStart -= BuffLifeCount;
    }
    void BuffLifeCount(PlayerData playerData)
    {
        if (RoundDuration != 0)
        {
            roundTimer += 1;
            if (roundTimer >= RoundDuration)
            {
                playerData.RemoveBuff(this);
            }
        }
    }
    #region Serialize
    protected virtual object[] GetSerializeVarArray()
    {
        return new object[] { };
    }
    public JSONNode Serialize()
    {
        string varString = "";
        var varArray = GetSerializeVarArray();

        if (varArray.Length > 0)
            varString += varArray[0].ToString();
        for (int i = 1; i < varArray.Length; i++)
        {
            varString += "," + varArray[i].ToString();
        }
        return ToString() + "(" + varString + ")";
    }
    public static Buff Deserialize(JSONNode json)
    {
        return ReflectionCreater.GetObject<Buff>(json);
    }
    #endregion
}
