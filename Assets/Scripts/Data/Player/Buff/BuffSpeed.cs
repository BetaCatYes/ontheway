﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuffSpeed : Buff
{
    int value;
    public BuffSpeed(int value)
    {
        this.value = value;
    }
    public BuffSpeed(int roundDuration, int value)
    {
        RoundDuration = roundDuration;
        this.value = value;
    }
    public override void OnCast(PlayerData playerData)
    {
        base.OnCast(playerData);
        playerData.moveIncreaseSpeed += value;
    }
    public override void OnRemove(PlayerData playerData)
    {
        base.OnRemove(playerData);
        playerData.moveIncreaseSpeed -= value;
    }
    protected override object[] GetSerializeVarArray()
    {
        return new object[] { RoundDuration, value };
    }
}
