﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardSpeedStarData : CardData
{
    public CardSpeedStarData()
    {
        energyCost = 1;
    }
    public override void Effect(MapData mapData, BlockData blockData, PlayerData playerData)
    {
        playerData.AddBuff(new BuffSpeedStar());
    }
}
