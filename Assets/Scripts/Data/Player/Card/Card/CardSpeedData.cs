﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardSpeedData : CardData
{
    public CardSpeedData()
    {
        energyCost = 1;
    }
    public override void Effect(MapData mapData, BlockData blockData, PlayerData playerData)
    {
        playerData.AddBuff(new BuffSpeed(1, 3));
    }
}
