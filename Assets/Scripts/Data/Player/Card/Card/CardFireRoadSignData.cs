﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardFireRoadSignData : CardData
{
    public CardFireRoadSignData()
    {
        energyCost = 2;
    }
    public override void Effect(MapData mapData, BlockData blockData, PlayerData playerData)
    {
        List<BlockData> blocks = new List<BlockData>();
        foreach (var b in mapData.blockDatas)
        {
            if (b.RoadSignCount > 0)
            {
                blocks.Add(b);
            }
        }

        int[] posIndexs = new int[blocks.Count];
        int[] fires = new int[blocks.Count];
        for (int i = 0; i < blocks.Count; i++)
        {
            posIndexs[i] = blocks[i].index;
            fires[i] = blocks[i].FireCount + blocks[i].RoadSignCount;
        }
        mapData.BlocksChange(posIndexs, null, null, fires);
    }
}
