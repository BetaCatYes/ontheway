﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardSpeedStarData2 : CardData
{
    public CardSpeedStarData2()
    {
        energyCost = 3;
    }
    public override void Effect(MapData mapData, BlockData blockData, PlayerData playerData)
    {
        playerData.AddBuff(new BuffSpeedStar2());
    }
}
