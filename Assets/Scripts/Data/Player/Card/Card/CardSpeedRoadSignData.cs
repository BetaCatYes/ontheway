﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardSpeedRoadSignData : CardData
{
    public CardSpeedRoadSignData()
    {
        energyCost = 1;
    }
    public override void Effect(MapData mapData, BlockData blockData, PlayerData playerData)
    {
        playerData.AddBuff(new BuffSpeedRoadSign());
    }
}

