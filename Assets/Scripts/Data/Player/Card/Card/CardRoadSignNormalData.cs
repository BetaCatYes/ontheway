﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardRoadSignNormalData : CardData
{
    public CardRoadSignNormalData()
    {
        energyCost = 1;
        selectRange = 5;
    }
    public override void Effect(MapData mapData, BlockData blockData, PlayerData playerData)
    {
        blockData.RoadSignCount += 3;
    }
}
