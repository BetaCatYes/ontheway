﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardMoveNormalData : CardData
{
    public CardMoveNormalData()
    {
        energyCost = 1;
    }
    public override void Effect(MapData mapData, BlockData blockData, PlayerData playerData)
    {
        MoveData.Effect(2, mapData, playerData);
    }
}
