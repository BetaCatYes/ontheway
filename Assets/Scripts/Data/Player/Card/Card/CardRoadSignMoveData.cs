﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardRoadSignMoveData : CardData
{
    public CardRoadSignMoveData()
    {
        energyCost = 2;
    }
    public override void Effect(MapData mapData, BlockData blockData, PlayerData playerData)
    {
        playerData.OnMovingStart += PutRoadSign;
        MoveData.Effect(3, mapData, playerData);
        playerData.OnMovingStart -= PutRoadSign;
    }
    void PutRoadSign(int posIndex, int? stepNumber, MapData mapData, PlayerData playerData)
    {
        mapData.blockDatas[posIndex].RoadSignCount++;
    }
}
