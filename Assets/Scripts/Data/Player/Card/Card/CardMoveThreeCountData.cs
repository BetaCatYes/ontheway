﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardMoveThreeCountData : CardData
{
    public CardMoveThreeCountData()
    {
        energyCost = 2;
    }
    public override void Effect(MapData mapData, BlockData blockData, PlayerData playerData)
    {
        for (int i = 0; i < 3; i++)
        {
            MoveData.Effect(1, mapData, playerData);
        }
    }
}
