﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardFireData2 : CardData
{
    public CardFireData2()
    {
        energyCost = 3;
    }
    public override void Effect(MapData mapData, BlockData blockData, PlayerData playerData)
    {
        List<BlockData> blocks = new List<BlockData>();
        for (int i = -3; i < 3; i++)
        {
            var b = mapData.GetBlockDataByIndex(i + playerData.posIndex);
            if (b != null)
                blocks.Add(b);
        }
        int[] posIndexs = new int[blocks.Count];
        int[] fires = new int[blocks.Count];
        for (int i = 0; i < blocks.Count; i++)
        {
            posIndexs[i] = blocks[i].index;
            fires[i] = blocks[i].FireCount + 2;
        }
        mapData.BlocksChange(posIndexs, null, null, fires);
    }
}
