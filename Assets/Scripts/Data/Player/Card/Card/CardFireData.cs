﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardFireData : CardData
{
    public CardFireData()
    {
        energyCost = 1;
        selectRange = 5;
    }
    public override void Effect(MapData mapData, BlockData blockData, PlayerData playerData)
    {
        blockData.FireCount += 2;
    }
}
