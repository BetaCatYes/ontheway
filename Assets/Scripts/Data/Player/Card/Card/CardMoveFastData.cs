﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardMoveFastData : CardData
{
    public CardMoveFastData()
    {
        energyCost = 2;
    }
    public override void Effect(MapData mapData, BlockData blockData, PlayerData playerData)
    {
        MoveData.Effect(5, mapData, playerData);
    }
}
