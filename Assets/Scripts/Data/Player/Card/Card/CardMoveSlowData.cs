﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardMoveSlowData : CardData
{
    public CardMoveSlowData()
    {
        energyCost = 0;
    }
    public override void Effect(MapData mapData, BlockData blockData, PlayerData playerData)
    {
        MoveData.Effect(1, mapData, playerData);
    }
}
