﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardSpeedRoadSignData2 : CardData
{
    public CardSpeedRoadSignData2()
    {
        energyCost = 0;
        selectRange = 5;
    }
    public override void Effect(MapData mapData, BlockData blockData, PlayerData playerData)
    {
        blockData.RoadSignCount += playerData.moveIncreaseSpeed;
    }
}

