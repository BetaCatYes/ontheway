﻿using System.Collections;
using System.Collections.Generic;
using SimpleJSON;
using UnityEngine;
[System.Serializable]
public class CardGroupData
{
    public List<CardData> handCards = new List<CardData>();//手牌堆
    public List<CardData> stackCards = new List<CardData>();//卡牌堆
    public List<CardData> scrapCards = new List<CardData>();//弃牌堆
    public event System.Action<List<CardData>> OnDrawCard;
    public void Init()
    {
        stackCards.Sort((x, y) => Random.Range(-1, 2));//乱序
    }
    public void EffectCard(CardData cardData, BlockData blockData, MapData mapData, PlayerData playerData)
    {
        handCards.Remove(cardData);
        scrapCards.Add(cardData);
        cardData.Effect(mapData, blockData, playerData);
    }
    public void DrawCards(int count)
    {
        var cards = new List<CardData>();
        for (int i = 0; i < count; i++)
        {
            cards.Add(DrawCard());
        }
        if (OnDrawCard != null)
            OnDrawCard.Invoke(cards);
    }
    CardData DrawCard()
    {
        if (stackCards.Count == 0)
        {
            stackCards.AddRange(scrapCards);
            stackCards.Sort((x, y) => Random.Range(-1, 2));//乱序
            scrapCards.Clear();
        }
        if (stackCards.Count == 0)
            return null;
        var card = stackCards[0];
        stackCards.RemoveAt(0);
        handCards.Add(card);
        return card;
    }
    #region Serialize
    public JSONNode Serialize()
    {
        JSONObject json = new JSONObject();

        var jsonHandCards = new JSONArray();
        for (int i = 0; i < handCards.Count; i++)
        {
            jsonHandCards[i] = handCards[i].Serialize();
        }
        json["handsCard"] = jsonHandCards;

        var jsonStackCards = new JSONArray();
        for (int i = 0; i < stackCards.Count; i++)
        {
            jsonStackCards[i] = stackCards[i].Serialize();
        }
        json["stackCards"] = jsonStackCards;

        var jsonScrapCards = new JSONArray();
        for (int i = 0; i < scrapCards.Count; i++)
        {
            jsonScrapCards[i] = scrapCards[i].Serialize();
        }
        json["scrapCards"] = jsonScrapCards;

        return json;
    }
    public static CardGroupData Deserialize(JSONNode json)
    {
        var cardGroupData = new CardGroupData();

        var jsonHandCards = json["handsCard"];
        for (int i = 0; i < jsonHandCards.Count; i++)
        {
            var card = CardData.Deserialize(jsonHandCards[i]);
            cardGroupData.handCards.Add(card);
        }

        var jsonStackCards = json["stackCards"];
        for (int i = 0; i < jsonStackCards.Count; i++)
        {
            var card = CardData.Deserialize(jsonStackCards[i]);
            cardGroupData.stackCards.Add(card);
        }

        var jsonScrapCards = json["scrapCards"];
        for (int i = 0; i < jsonScrapCards.Count; i++)
        {
            var card = CardData.Deserialize(jsonScrapCards[i]);
            cardGroupData.scrapCards.Add(card);
        }

        return cardGroupData;
    }
    #endregion
}
