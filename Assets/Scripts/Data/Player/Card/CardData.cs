﻿using System.Collections;
using System.Collections.Generic;
using SimpleJSON;
using UnityEngine;
[System.Serializable]
public abstract class CardData
{
    public int energyCost;
    public int selectRange;
    public abstract void Effect(MapData mapData, BlockData blockData, PlayerData playerData);
    #region Serialize
    protected virtual object[] GetSerializeVarArray()
    {
        return new object[] { };
    }
    public JSONNode Serialize()
    {
        string varString = "";
        var varArray = GetSerializeVarArray();

        if (varArray.Length > 0)
            varString += varArray[0].ToString();
        for (int i = 1; i < varArray.Length; i++)
        {
            varString += "," + varArray[i].ToString();
        }
        return ToString() + "(" + varString + ")";
    }
    public static CardData Deserialize(JSONNode json)
    {
        return ReflectionCreater.GetObject<CardData>(json);
    }
    #endregion
}