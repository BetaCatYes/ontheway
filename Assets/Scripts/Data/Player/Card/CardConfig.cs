﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "CardConfig", menuName = "Custom/CardConfig")]
public class CardConfig : ScriptableObject
{
    public string[] cardNames;
}
