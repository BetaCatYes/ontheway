﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIData
{
    public AIData()
    {
    }
    public int BattleAssess(BattleData battleData)
    {
        return battleData.playerDatas[1].posIndex - battleData.playerDatas[0].posIndex;
    }
    public void Select(BattleData battleData, int playerIndex, List<int> resultIndexs, List<int> resultRanges)
    {
        var cards = new List<CardData>();
        cards.AddRange(battleData.playerDatas[playerIndex].cardGroupData.handCards);

        //顺序排列
        List<List<int>> indexResList = new List<List<int>>();
        List<int> indexList = new List<int>();
        for (int i = 0; i < cards.Count; i++)
        {
            indexList.Add(i);
        };
        Permutation(indexResList, indexList, 0);

        //出牌后选择位置排列
        List<List<int>> rangeResList = new List<List<int>>();
        for (int i = -cards[0].selectRange; i <= cards[0].selectRange; i++)
        {
            var newList = new List<int>() { i };
            rangeResList.Add(newList);
        }
        for (int i = 1; i < cards.Count; i++)
        {
            int count = rangeResList.Count;
            var tempList = new List<List<int>>();
            tempList.AddRange(rangeResList);
            for (int j = -cards[i].selectRange; j <= cards[i].selectRange; j++)
            {
                if (j != 0)
                {
                    for (int k = 0; k < count; k++)
                    {
                        var newList = new List<int>();
                        newList.AddRange(rangeResList[k]);
                        rangeResList.Add(newList);
                    }
                }
            }
            for (int l = 0, j = -cards[i].selectRange; j <= cards[i].selectRange; l++, j++)
            {
                for (int k = 0; k < count; k++)
                {
                    rangeResList[j * l + k].Add(j);
                }
            }
        }
        //计算最佳选择
        var jsonBattleData = battleData.Serialize();
        int maxScore = int.MinValue;
        int maxIndexListIndex = 0;
        int maxRangeListIndex = 0;

        if (indexResList.Count == 0 || rangeResList.Count == 0) return;

        for (int i = 0; i < indexResList.Count; i++)
        {
            for (int j = 0; j < rangeResList.Count; j++)
            {
                var tempBattleData = BattleData.Deserialize(jsonBattleData);
                for (int k = 0; k < cards.Count; k++)
                {
                    var card = cards[indexResList[i][k]];
                    if (tempBattleData.CanCardEffect(card))
                    {
                        var block = tempBattleData.mapData.GetBlockDataByIndex(tempBattleData.PlayerData.posIndex + rangeResList[j][k]);
                        if (block != null)
                            tempBattleData.CardEffect(card, block);
                    }
                }
                var score = BattleAssess(tempBattleData);
                if (maxScore < score)
                {
                    maxScore = score;
                    maxIndexListIndex = i;
                    maxRangeListIndex = j;
                }
            }
        }
        resultIndexs.AddRange(indexResList[maxIndexListIndex]);
        resultRanges.AddRange(rangeResList[maxRangeListIndex]);
    }
    #region 全排列
    void Permutation(List<List<int>> res, List<int> num, int index)
    {
        if (index >= num.Count)
        {
            res.Add(num);
            return;
        }
        for (int i = index; i < num.Count; i++)
        {
            Swap(num, i, index);
            var newNum = new List<int>();
            newNum.AddRange(num);
            Permutation(res, newNum, index + 1);
            Swap(num, index, i);
        }
    }
    void Swap(List<int> list, int fIndex, int sIndex)
    {
        var temp = list[fIndex];
        list[fIndex] = list[sIndex];
        list[sIndex] = temp;
    }
    #endregion
}
