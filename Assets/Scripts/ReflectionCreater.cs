﻿using System.Collections.Generic;
using System;
using System.Reflection;
using System.Text.RegularExpressions;

public class ReflectionCreater
{
    public static T GetObject<T>(string name)
    {
        T buff = default(T);
        if (string.IsNullOrEmpty(name)) return buff;

        Assembly ass = Assembly.GetExecutingAssembly();
        string className = Regex.Match(name, @"\w+(?=\()|\w+").Value;
        //string valueStr = Regex.Match(buffName, @"(?<=\()-?\d+").Value;
        string valueStr = Regex.Match(name, @"(?<=\()(.+?)(?=\))").Value;

        Type classType = ass.GetType(className);
        if (classType == null)
            UnityEngine.Debug.Log("不存在" + className + "这个Buff");
        if (valueStr != "")
        {
            var values = valueStr.Split(',');
            List<Object> args = new List<Object>();
            for (int i = 0; i < values.Length; i++)
            {
                Object value;
                try { value = int.Parse(values[i]); }
                catch { value = float.Parse(values[i]); }
                args.Add(value);
            }
            buff = (T)Activator.CreateInstance(classType, args.ToArray());
        }
        else
        {
            buff = (T)Activator.CreateInstance(classType);
        }
        return buff;
    }
}
